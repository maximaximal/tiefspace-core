CWorldLoop = nil;
CPlugin = nil;

function initialize(Plugin)
	CWorldLoop = Plugin:getWorldLoop()
    CPlugin = Plugin

    Plugin:registerChatCommand("buildSpaceship", "onBuildSpaceship")
    Plugin:registerChatCommand("kickMe", "onKickMe")
end
